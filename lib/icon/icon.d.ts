/// <reference types="react" />
import './icon.module.scss';
export interface IconProps {
    fill?: string;
    name: string;
    klass?: string;
}
export declare function Icon(props: IconProps): JSX.Element;
export default Icon;
